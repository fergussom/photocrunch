/**
 * Kanji rendering and I/O engine - image reader/writer definitions
 *
 * Copyright (C) 2010-2015 Emmanuel Marty
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef  _PSD_LAYERIMAGE_H
#define  _PSD_LAYERIMAGE_H

/* Include definitions */
#include "Common.h"

/**
 * Class for loading and saving image files
 */

class PsdLayerImage {
public:
   /** Constructor */
   PsdLayerImage ();

   /** Destructor */
   ~PsdLayerImage ();

   /** Free resources used by this image */
   void freeImage (void);

   /**
    * Get this image's width
    *
    * @return width in pixels
    */
   long getImageWidth (void) const;

   /**
    * Get this image's height
    *
    * @return height in pixels
    */
   long getImageHeight (void) const;

   /**
    * Get this image's contents, as an array of ABGR pixels
    *
    * @return pixel data
    */
   unsigned int *getPixels (void) const;

   /**
    * Set this image's contents, as an array of ABGR pixels
    *
    * @param nWidth image width, in pixels
    * @param nHeight image height, in pixels
    * @param bAlphaChannelPresent true if alpha channel is present, false if not
    * @param lpPixels image contents, as an array of ABGR pixels; the buffer is not copied but will be freed when this image is destroyed
    * @param bGreyscale true if image was created from greyscale data
    * @param bHiQuality true if image was created from 8 bits per color channel data, false it was created from less (for instance 15/16 bits per pixel images)
    */
   void setPixels (unsigned long nWidth, unsigned long nHeight, bool bAlphaChannelPresent, unsigned int *lpPixels, bool bGreyscale = false, bool bHiQuality = true);
   
   /** Clear pixels that are entirely invisible, so as to save a smaller compressed image */
   void optimizeImage(void);

   /**
    * Trim image to the part that contains visible pixels
    *
    * @param nPadding number of padding pixels to add around the trimmed image, if any
    * @param rx1 returned leftmost coordinate of trimmed region, in original image coordinates
    * @param ry1 returned topmost coordinate of trimmed region, in original image coordinates
    * @param rx2 returned rightmost coordinate of trimmed region, in original image coordinates, +1
    * @param ry2 returned bottommost coordinate of trimmed region, in original image coordinates, +1
    */
   void trimImage(long nPadding, long &rx1, long &ry1, long &rx2, long &ry2);

private:
   /** Image width, in pixels */
   long _nImageWidth;

   /** Image height, in pixels */
   long _nImageHeight;

   /** true if image was created from greyscale data */
   bool _bGreyscale;

   /** true if image was created from 8 bits per color channel data, false it was created from less (for instance 15/16 bits per pixel images) */
   bool _bHiQuality;

   /** true if image contains an alpha channel */
   bool _bAlphaChannelPresent;

   /** Array of image pixels */
   unsigned int *_lpPixels;
};

#endif   /* _PSD_LAYERIMAGE_H */

/**
 * Main
 *
 * Copyright (C) 2010-2015 Emmanuel Marty
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Common.h"
#include "PsdDocument.h"
#include "png.h"
#include "tinyxml2.h"

#ifndef MAX_PATH
#define MAX_PATH 260
#endif

/**
 * Save image as png
 *
 * @param lpImage image to save
 * @param lpszFilename output filename
 *
 * @return true for success, false for failure
 */

bool saveAsPng(const PsdLayerImage *lpImage, const char *lpszFilename) {
   int code = 0;
   FILE *fp = NULL;
   png_structp png_ptr = NULL;
   png_infop info_ptr = NULL;
   png_bytep row = NULL;

   if (!lpImage || !lpImage->getImageWidth() || !lpImage->getImageHeight()) return true;

   fp = fopen(lpszFilename, "wb");
   if (!fp) {
      fprintf(stderr, "Error opening %s for writing\n", lpszFilename);
      return false;
   }

   png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
   if (png_ptr == NULL) {
      fprintf(stderr, "Error writing %s\n", lpszFilename);
      fclose(fp);
      return false;
   }

   // Initialize info structure
   info_ptr = png_create_info_struct(png_ptr);
   if (info_ptr == NULL) {
      fprintf(stderr, "Error writing %s\n", lpszFilename);
      png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
      fclose(fp);
      return false;
   }

   if (setjmp(png_jmpbuf(png_ptr))) {
      fprintf(stderr, "Error writing %s\n", lpszFilename);
      png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
      png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
      fclose(fp);
      return false;
   }

   png_init_io(png_ptr, fp);

   png_set_IHDR(png_ptr, info_ptr, lpImage->getImageWidth(), lpImage->getImageHeight(),
      8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
      PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

   png_write_info(png_ptr, info_ptr);

   row = (png_bytep)malloc(4 * lpImage->getImageWidth() * sizeof(png_byte));

   int x, y;
   for (y = 0; y<lpImage->getImageHeight(); y++) {
      const unsigned int *lpPixels = lpImage->getPixels() + y * lpImage->getImageWidth();
      
      for (x = 0; x < lpImage->getImageWidth(); x++) {
         unsigned int nPixel = *lpPixels++;

         row[x * 4] = nPixel & 0xff;
         row[x * 4 + 1] = (nPixel >> 8) & 0xff;
         row[x * 4 + 2] = (nPixel >> 16) & 0xff;
         row[x * 4 + 3] = (nPixel >> 24) & 0xff;
      }

      png_write_row(png_ptr, row);
   }

   png_write_end(png_ptr, NULL);

   free(row);
   png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
   png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
   fclose(fp);

   return true;
}

/**
 * Entry point
 *
 * @param argc number of command-line arguments including program name
 * @param argv array of command-line arguments
 *
 * @return process result (0 for success)
 */

int main (int argc, char **argv) {
   const char *lpszPsdFilename = NULL;
   const char *lpszOutputFolderName = NULL;
   PsdDocument *lpPsdDocument;
   void *lpPsdData;
   size_t nPsdSize;
   bool bArgsError = false, bSuccess;
   int i, nResult;
   long nCurLayer;
   tinyxml2::XMLDocument *lpXmlDocument;
   tinyxml2::XMLElement *lpXmlRoot;
   char szFormat[256];
   char szOutputPath[MAX_PATH];

   /* Parse command-line arguments */

   for (i = 1; i < argc; i++) {
      if (!strcmp(argv[i], "-i")) {
         /* Psd filename specified */
         if ((i + 1) < argc) {
            if (!lpszPsdFilename)
               lpszPsdFilename = argv[i + 1];
            else
               bArgsError = true;
            i++;
         }
         else
            bArgsError = true;
      }
      else if (!strcmp(argv[i], "-o")) {
         /* Output folder specified */
         if ((i + 1) < argc) {
            if (!lpszOutputFolderName)
               lpszOutputFolderName = argv[i + 1];
            else
               bArgsError = true;
            i++;
         }
         else
            bArgsError = true;
      }
      else {
         /* Unknown option */
         bArgsError = true;
         break;
      }
   }

   if (!lpszPsdFilename || !lpszOutputFolderName || bArgsError) {
      /* Invalid or missing arguments, bail */
      fprintf(stderr, "Usage: %s -i [input.psd] -o [output folder]\n", argv[0]);
      exit(100);
   }

   lpPsdDocument = new PsdDocument();

   /* Mmap the psd file */

#if defined (_WIN32)
   HANDLE hPsdFile = CreateFileA(lpszPsdFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
   if (hPsdFile == INVALID_HANDLE_VALUE) {
      fprintf(stderr, "Unable to open %s for input\n", lpszPsdFilename);
      exit(100);
   }

   nPsdSize = GetFileSize(hPsdFile, NULL);
   if (!nPsdSize || nPsdSize == -1L) {
      fprintf(stderr, "%s is empty\n", lpszPsdFilename);
      exit(100);
   }

   HANDLE hPsdMapping = CreateFileMapping(hPsdFile, NULL, PAGE_READONLY, 0L, 0L, NULL);
   if (!hPsdMapping) {
      CloseHandle(hPsdFile);
      fprintf(stderr, "Unable to memory-map %s for input\n", lpszPsdFilename);
      exit(100);
   }

   lpPsdData = MapViewOfFile(hPsdMapping, FILE_MAP_READ, 0L, 0L, 0L);
   if (!lpPsdData) {
      CloseHandle(hPsdMapping);
      CloseHandle(hPsdFile);
      fprintf(stderr, "Unable to memory-map %s for input\n", lpszPsdFilename);
      exit(100);
   }
#elif defined (__APPLE__)
   int fd = open (lpszPsdFilename, 0);
   
   if (fd < 0) {
      fprintf(stderr, "Unable to open %s for input\n", lpszPsdFilename);
      exit(100);
   }
   
   lseek (fd, 0, SEEK_END);
   nPsdSize = lseek (fd, 0, SEEK_CUR);
   lseek (fd, 0, SEEK_SET);
   
   lpPsdData = mmap (NULL, nPsdSize, PROT_READ, MAP_PRIVATE, fd, 0L);
   if (!lpPsdData || lpPsdData == ((void *) -1)) {
      close (fd);
      fprintf(stderr, "Unable to memory-map %s for input\n", lpszPsdFilename);
      exit(100);
   }
#else /* defined (_WIN32) */
#error Unsupported platform, please add a file memory mapping implementation here
#endif /* defined (_WIN32) */

   /* Process the psd */

   lpXmlDocument = new tinyxml2::XMLDocument(true, tinyxml2::PRESERVE_WHITESPACE);

   bSuccess = lpPsdDocument->parsePsd((const unsigned char *) lpPsdData, nPsdSize);
   if (bSuccess) {
      nResult = 0;

      lpXmlRoot = lpXmlDocument->NewElement("psd");

      /* Put global psd info into the xml */

      snprintf(szFormat, 255, "%ld", lpPsdDocument->getWidth());
      szFormat[255] = 0;
      lpXmlRoot->SetAttribute("width", szFormat);

      snprintf(szFormat, 255, "%ld", lpPsdDocument->getHeight());
      szFormat[255] = 0;
      lpXmlRoot->SetAttribute("height", szFormat);

      lpXmlDocument->InsertEndChild(lpXmlRoot);

      /* Save each layer */

      for (nCurLayer = 0; bSuccess && nCurLayer < lpPsdDocument->getLayerCount(); nCurLayer++) {
         const PsdLayer *lpLayerInfo = lpPsdDocument->getLayerInfo (nCurLayer);
         PsdLayerImage outputImage;
         unsigned int *lpLayerPixels;
         long nLayerWidth = lpPsdDocument->getWidth();
         long nLayerHeight = lpPsdDocument->getHeight();
         long rx1 = 0, ry1 = 0, rx2 = 0, ry2 = 0;
         char szOutputFilename[MAX_PATH];

         /* Put layer contents seperately into an image */

         lpLayerPixels = new unsigned int[nLayerWidth * nLayerHeight];
         outputImage.setPixels(nLayerWidth, nLayerHeight, true, lpLayerPixels, false, true);
         memset(lpLayerPixels, 0, nLayerWidth * nLayerHeight * sizeof(unsigned int));
         lpPsdDocument->compositeLayer(&outputImage, nCurLayer, true);

         /* Set fully transparent pixels to black */
         outputImage.optimizeImage();

         /* Trim it to the minimum rectangle that has visible pixels */
         outputImage.trimImage(0 /* padding */, rx1, ry1, rx2, ry2);

         strncpy(szOutputFilename, lpLayerInfo->szName, MAX_PATH - 1);
         szOutputFilename[MAX_PATH - 1] = 0;
         for (i = 0; szOutputFilename[i]; i++) {
            if (szOutputFilename[i] == '/' || szOutputFilename[i] == '\\' || szOutputFilename[i] == ':' || szOutputFilename[i] == '<' || szOutputFilename[i] == '>')
               szOutputFilename[i] = '_';
         }

#ifdef _WIN32
         snprintf(szOutputPath, MAX_PATH - 1, "%s\\%s.png", lpszOutputFolderName, szOutputFilename);
#else
         snprintf(szOutputPath, MAX_PATH - 1, "%s/%s.png", lpszOutputFolderName, szOutputFilename);
#endif
         szOutputPath[MAX_PATH - 1] = 0;

         /* Save trimmed contents as png */
         bSuccess = saveAsPng(&outputImage, szOutputPath);

         if (bSuccess) {
            tinyxml2::XMLElement *lpXmlLayerElement = lpXmlDocument->NewElement("layer");

            /* Write info about this layer in the xml */

            if (lpLayerInfo->bLayerIdKnown) {
               snprintf(szFormat, 255, "%ld", lpLayerInfo->nLayerId);
               szFormat[255] = 0;
               lpXmlLayerElement->SetAttribute("id", szFormat);
            }

            lpXmlLayerElement->SetAttribute("name", lpLayerInfo->szName);

            snprintf(szFormat, 255, "%c%c%c%c",
                     (int)(lpLayerInfo->nBlendingMode >> 24),
                     (int)((lpLayerInfo->nBlendingMode >> 16) & 255),
                     (int)((lpLayerInfo->nBlendingMode >> 8) & 255),
                     (int)(lpLayerInfo->nBlendingMode & 255));
            szFormat[255] = 0;
            lpXmlLayerElement->SetAttribute("blending_mode", szFormat);

            snprintf(szFormat, 255, "%ld", lpLayerInfo->nOpacity);
            szFormat[255] = 0;
            lpXmlLayerElement->SetAttribute("opacity", szFormat);

            snprintf(szFormat, 255, "%ld", rx1);
            szFormat[255] = 0;
            lpXmlLayerElement->SetAttribute("x", szFormat);

            snprintf(szFormat, 255, "%ld", ry1);
            szFormat[255] = 0;
            lpXmlLayerElement->SetAttribute("y", szFormat);

            snprintf(szFormat, 255, "%ld", rx2 - rx1);
            szFormat[255] = 0;
            lpXmlLayerElement->SetAttribute("width", szFormat);

            snprintf(szFormat, 255, "%ld", ry2 - ry1);
            szFormat[255] = 0;
            lpXmlLayerElement->SetAttribute("height", szFormat);

            if (lpLayerInfo->nParentLayerIdx != -1) {
               const PsdLayer *lpParentLayerInfo = lpPsdDocument->getLayerInfo(lpLayerInfo->nParentLayerIdx);

               if (lpParentLayerInfo->bLayerIdKnown) {
                  snprintf(szFormat, 255, "%ld", lpParentLayerInfo->nLayerId);
                  szFormat[255] = 0;

                  lpXmlLayerElement->SetAttribute("parent_id", szFormat);
               }
            }

            lpXmlRoot->InsertEndChild(lpXmlLayerElement);
         }
      }
   }
   else {
      nResult = 100;
      fprintf(stderr, "Error processing %s as a PSD document\n", argv[1]);
   }

#ifdef _WIN32
   snprintf(szOutputPath, MAX_PATH - 1, "%s\\info.xml", lpszOutputFolderName);
#else
   snprintf(szOutputPath, MAX_PATH - 1, "%s/info.xml", lpszOutputFolderName);
#endif

   /* Write XML with all the layer info */

   if (lpXmlDocument->SaveFile(szOutputPath, false) != tinyxml2::XML_NO_ERROR) {
      nResult = 100;
      fprintf(stderr, "Error writing %s\n", szOutputPath);
   }

   delete lpXmlDocument;
   lpXmlDocument = NULL;

   /* Free mmap */

#if defined (_WIN32)
   UnmapViewOfFile(lpPsdData);
   lpPsdData = NULL;
   CloseHandle(hPsdMapping);
   hPsdMapping = 0;
   CloseHandle(hPsdFile);
   hPsdFile = 0;
#elif defined (__APPLE__)
   munmap(lpPsdDocument, nPsdSize);
#else /* defined (_WIN32) */
#error Unsupported platform, please add a file memory unmapping implementation here
#endif /* defined (_WIN32) */

   return nResult;
}
